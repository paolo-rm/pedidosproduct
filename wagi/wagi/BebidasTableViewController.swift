//
//  BebidasTableViewController.swift
//  wagi
//
//  Created by Paolo Ramos on 6/5/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class BebidasTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    let arrayEstado = ["Recibí tu pedido", "Alistando tu pedido", "Llevando tu pedido", "Estamos en tu puerta"]
    let arrayPosition = [["lat": -16.539879, "long": -68.083965],
                         ["lat": -16.509372, "long": -68.123867],
                         ["lat": -16.499941, "long": -68.134889]]
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductoViewController") as! ProductoViewController
        let estado = self.arrayEstado[Int.random(in: 0..<4)]
        let position = self.arrayPosition[Int.random(in: 0..<3)]
        vc.estado = estado
        vc.lat = position["lat"] ?? 0.0
        vc.lon = position["long"] ?? 0.0
        switch indexPath.row {
        case 0:
            vc.descripcion = "Coca Cola (3 lts) \nBs 10"
            break
        case 1:
            vc.descripcion = "Ron Abuelo \nBs 85"
            break
        case 2:
            vc.descripcion = "Agua (2Lts) \nBs 6"
            break
        default:
            break
        }
        self.navigationController?.show(vc, sender: nil)
    }
}
