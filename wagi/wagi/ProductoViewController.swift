//
//  ProductoViewController.swift
//  wagi
//
//  Created by Paolo Ramos on 6/5/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit
import MapKit

class ProductoViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var estadoBtn: UIButton!
    @IBOutlet weak var desc: UILabel!
    
    var descripcion: String = ""
    var estado: String = ""
    var lat = -16.532838
    var lon = -68.100957
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.desc.text = self.descripcion
        self.estadoBtn.setTitle(self.estado, for: .normal)
        
        let mapCenter = CLLocationCoordinate2DMake(self.lat, self.lon)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: mapCenter, span: span)
        self.map.region = region
        
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(latitude: self.lat, longitude: self.lon)
        annotation.coordinate = centerCoordinate
        annotation.title = self.descripcion
        self.map.addAnnotation(annotation)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
