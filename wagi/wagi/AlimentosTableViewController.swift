//
//  AlimentosTableViewController.swift
//  wagi
//
//  Created by Paolo Ramos on 6/5/19.
//  Copyright © 2019 paolo. All rights reserved.
//

import UIKit

class AlimentosTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    let arrayEstado = ["Recibí tu pedido", "Alistando tu pedido", "Llevando tu pedido", "Estamos en tu puerta"]
    let arrayPosition = [["lat": -16.539879, "long": -68.083965],
                     ["lat": -16.509372, "long": -68.123867],
                     ["lat": -16.499941, "long": -68.134889]]
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductoViewController") as! ProductoViewController
        let estado = self.arrayEstado[Int.random(in: 0..<4)]
        let position = self.arrayPosition[Int.random(in: 0..<3)]
        vc.estado = estado
        vc.lat = position["lat"] ?? 0.0
        vc.lon = position["long"] ?? 0.0
        switch indexPath.row {
        case 0:
            vc.descripcion = "Carnes (pulpa)\n1 kg    Bs 40"
            break
        case 1:
            vc.descripcion = "Abarrotes (arroz)\n1 kg    Bs 6"
            break
        case 2:
            vc.descripcion = "Repostería (pan)\n1 kg    Bs 1"
            break
        default:
            break
        }
        self.navigationController?.show(vc, sender: nil)
    }

}
